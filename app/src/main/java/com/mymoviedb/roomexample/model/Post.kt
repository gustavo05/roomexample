package com.mymoviedb.roomexample.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "posts",
    foreignKeys = [ForeignKey(entity = User::class, parentColumns = ["id"], childColumns = ["userId"])]
)
data class Post(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val userId: Long,
    val description: String,
    val imageUrl: String,
    val likes: Int
)