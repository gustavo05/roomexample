package com.mymoviedb.roomexample.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "comments",
    foreignKeys = [
        ForeignKey(
            entity = User::class,
            parentColumns = ["id"],
            childColumns = ["userId"]
        ),
        ForeignKey(
            entity = Post::class,
            parentColumns = ["id"],
            childColumns = ["postId"]
        )]
)
data class Comment(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val userId: Long,
    val postId: Long,
    val comment: String,
    val likes: Int
)