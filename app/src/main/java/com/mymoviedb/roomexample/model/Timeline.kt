package com.mymoviedb.roomexample.model

import androidx.room.DatabaseView

@DatabaseView(
    "Select us.id as userId, us.name, us.lastName, us.photoUrl, po.id as postId, po.imageUrl, po.description, po.likes FROM users as us INNER JOIN posts as po ON us.id = po.userId"
)
data class Timeline(
    val userId: Long,
    val name: String,
    val lastName: String,
    val photoUrl: String? = "",
    val postId: Long,
    val imageUrl: String? = "",
    val description: String? = "",
    val likes: Int
)