package com.mymoviedb.roomexample.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "users", indices = [Index(unique = true, value = ["email"])])
data class User(
    @PrimaryKey(autoGenerate = true) var id: Long,
    val name: String,
    val lastName: String,
    var email: String,
    val photoUrl: String? = ""
)