package com.mymoviedb.roomexample.model

data class PostComment(
    val userId: Int,
    val name: String,
    val lastName: String,
    val photoUrl: String,
    val commentId: Int,
    val comment: String,
    val likes: Int
)