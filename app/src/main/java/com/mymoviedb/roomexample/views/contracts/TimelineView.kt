package com.mymoviedb.roomexample.views.contracts

import com.mymoviedb.roomexample.model.Timeline

interface TimelineView {
    fun postAdded()
}