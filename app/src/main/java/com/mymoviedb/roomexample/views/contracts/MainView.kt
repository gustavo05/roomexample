package com.mymoviedb.roomexample.views.contracts

interface MainView {
    fun showTimeline()
}