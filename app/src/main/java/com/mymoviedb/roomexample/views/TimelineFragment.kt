package com.mymoviedb.roomexample.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.mymoviedb.roomexample.R
import com.mymoviedb.roomexample.adapter.TimelineAdapter
import com.mymoviedb.roomexample.data.AppDatabase
import com.mymoviedb.roomexample.model.Timeline
import com.mymoviedb.roomexample.presenter.TimelinePresenter
import com.mymoviedb.roomexample.views.contracts.TimelineView
import kotlinx.android.synthetic.main.fragment_timeline.*
import kotlinx.android.synthetic.main.fragment_timeline.view.*

class TimelineFragment : Fragment(), TimelineView {

    private lateinit var presenter: TimelinePresenter
    private lateinit var timelineAdapter: TimelineAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_timeline, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.fab.setOnClickListener{
            presenter.insertPost()
        }
        initPresenter()
        initRecyclerView()
        fetchData()
    }

    private fun initRecyclerView() {
        timelineAdapter = TimelineAdapter(listOf())
        recyclerViewTimeline.apply {
            adapter = timelineAdapter
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
        }
    }

    private fun fetchData() {
        presenter.timelineLiveData.observe(this, Observer {
            timelineAdapter.loadData(it)
        })
    }

    private fun initPresenter() {
        val database = AppDatabase.getDatabase(context?.applicationContext!!)
        presenter = TimelinePresenter(this, database)
    }

    override fun postAdded() {
        recyclerViewTimeline.smoothScrollToPosition(timelineAdapter.itemCount -1)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}
