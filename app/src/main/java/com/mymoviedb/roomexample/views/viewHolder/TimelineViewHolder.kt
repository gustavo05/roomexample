package com.mymoviedb.roomexample.views.viewHolder

import android.view.View
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.mymoviedb.roomexample.model.Timeline
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.timeline_cell.view.*

class TimelineViewHolder(view:View):ViewHolder(view) {

    fun bind(timeline: Timeline) {
        with(itemView) {
            this.postDescription.text = timeline.description
            this.postLikesTextView.text = timeline.likes.toString()
            Picasso.get().load(timeline.imageUrl).into(this.postImageView)
        }
    }
}