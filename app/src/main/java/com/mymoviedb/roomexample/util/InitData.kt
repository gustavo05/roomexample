package com.mymoviedb.roomexample.util

import com.mymoviedb.roomexample.model.Post
import com.mymoviedb.roomexample.model.User

object InitData {
    val users = listOf(
        User(
            id = 0,
            name = "Gustavo",
            lastName = "Quesada",
            email = "quesada.tavo@gmail.com",
            photoUrl = "https://pbs.twimg.com/profile_images/989032648289980416/GEow6hTN.jpg"
        ),
        User(
            id = 0,
            name = "Juan Jose",
            lastName = "Quesada",
            email = "ajuanjo@gmail.com",
            photoUrl = "https://media.licdn.com/dms/image/C5603AQH6wIu5LommvA/profile-displayphoto-shrink_800_800/0?e=1571875200&v=beta&t=BJSt7Ea70H7a_0z3YqUOaKbicvsrRWLyfBn_5I1n0gI"
        )
    )
    val posts = listOf(
        Post(
            id = 0,
            userId = 1,
            description = "Amazing Place",
            imageUrl = "https://www.villascostarica.com/userfiles/playa-manuel-antonio.png",
            likes = 50
        ),
        Post(
            id = 0,
            userId = 2,
            imageUrl = "https://www.villascostarica.com/wp-content/uploads/2018/09/fi-flamingo-920x550.jpg",
            likes = 90,
            description = "Best experience ever"
        ),
        Post(
            id = 0,
            userId = 1,
            description = "A very friendly place",
            imageUrl = "https://www.villascostarica.com/wp-content/uploads/2018/09/fi-tamarindo-920x550.jpg",
            likes = 20
        ),
        Post(
            id = 0,
            userId = 2,
            imageUrl = "https://www.villascostarica.com/wp-content/uploads/2018/09/fi-dominical-920x550.jpg",
            likes = 40,
            description = "Not Bad!!"
        ),
        Post(
            id = 0,
            userId = 2,
            description = "Want to be here forever",
            imageUrl = "https://89675e09f433cad538fb4f31-mytanfeetcom.netdna-ssl.com/wp-content/uploads/2018/01/Things-to-do-in-Puerto-Viejo-beach-hop.jpg",
            likes = 35
        ),
        Post(
            id = 0,
            userId = 2,
            imageUrl = "https://www.cahuita.cr/wp-content/uploads/2019/06/20190608-142054-Costa-Rica-Cahuita-Playa-Parque-Nacional-IMG_0491-L3-1024x683.jpeg",
            likes = 12,
            description = "Amazing Place"
        ),
        Post(
            id = 0,
            userId = 1,
            description = "Amazing Place",
            imageUrl = "https://www.budget.co.cr/wp-content/uploads/locations-playa-jaco-facebook.jpg",
            likes = 24
        ),
        Post(
            id = 0,
            userId = 2,
            imageUrl = "https://costarica.org/wp-content/uploads/2017/09/Naranjo6.jpg",
            likes = 90,
            description = "Best place for surfing"
        )
    )

    val newPost = Post(
        id = 0,
        userId = 1,
        imageUrl = "https://www.larepublica.net/storage/images/2015/12/22/201512221027401.Chirripo.jpg",
        likes = 3,
        description = "Hiking"
    )
}
