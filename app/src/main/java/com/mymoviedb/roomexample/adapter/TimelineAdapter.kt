package com.mymoviedb.roomexample.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.mymoviedb.roomexample.R
import com.mymoviedb.roomexample.model.Timeline
import com.mymoviedb.roomexample.views.viewHolder.TimelineViewHolder

class TimelineAdapter(private var timeline: List<Timeline>) : Adapter<TimelineViewHolder>() {

    fun loadData(newData: List<Timeline>) {
        timeline = newData
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimelineViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.timeline_cell, parent, false)
        return TimelineViewHolder(view)
    }

    override fun getItemCount() = timeline.size

    override fun onBindViewHolder(holder: TimelineViewHolder, position: Int) = holder.bind(timeline[position])
}