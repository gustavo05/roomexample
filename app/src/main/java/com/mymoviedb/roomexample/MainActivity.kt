package com.mymoviedb.roomexample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mymoviedb.roomexample.data.AppDatabase
import com.mymoviedb.roomexample.presenter.MainPresenter
import com.mymoviedb.roomexample.views.TimelineFragment
import com.mymoviedb.roomexample.views.contracts.MainView

class MainActivity : AppCompatActivity(), MainView {

    private lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initPresenter()
        presenter.hasDataSaved()
    }

    override fun showTimeline() {
        val fragment = TimelineFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.mainContainer, fragment)
            .commit()
    }

    private fun initPresenter() {
        val database = AppDatabase.getDatabase(applicationContext)
        presenter = MainPresenter(this, database)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}
