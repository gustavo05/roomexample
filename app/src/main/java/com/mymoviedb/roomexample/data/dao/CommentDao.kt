package com.mymoviedb.roomexample.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mymoviedb.roomexample.model.Comment
import com.mymoviedb.roomexample.model.PostComment

@Dao
interface CommentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(comment: Comment): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(comment: List<Comment>)

    @Query("SELECT user.id as userId, user.name, user.lastName, user.photoUrl, comment.id as commentId, comment.comment, comment.likes FROM users as user INNER JOIN comments as comment ON user.id = comment.userId INNER JOIN posts as post  ON comment.postId = post.id Where post.id = :postId")
    fun getCommentsByPost(postId: Int): List<PostComment>
}