package com.mymoviedb.roomexample.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mymoviedb.roomexample.data.dao.CommentDao
import com.mymoviedb.roomexample.data.dao.PostDao
import com.mymoviedb.roomexample.data.dao.TimelineDao
import com.mymoviedb.roomexample.data.dao.UserDao
import com.mymoviedb.roomexample.model.Comment
import com.mymoviedb.roomexample.model.Post
import com.mymoviedb.roomexample.model.Timeline
import com.mymoviedb.roomexample.model.User

@Database(
    entities = [User::class, Post::class, Comment::class],
    views = [Timeline::class],
    version = AppDatabase.DATABASE_VERSION
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getUserDao(): UserDao
    abstract fun getPostDao(): PostDao
    abstract fun getCommentDao(): CommentDao
    abstract fun getTimelineDao(): TimelineDao


    companion object {
        private const val DATABASE_NAME = "room_database"
        const val DATABASE_VERSION = 1
        @Volatile
        private var INSTANCE: AppDatabase? = null



        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    DATABASE_NAME
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}