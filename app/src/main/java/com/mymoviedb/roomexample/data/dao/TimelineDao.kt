package com.mymoviedb.roomexample.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.mymoviedb.roomexample.model.Timeline

@Dao
interface TimelineDao {

    @Query("SELECT * FROM Timeline")
    fun getTimeline(): LiveData<List<Timeline>>
}