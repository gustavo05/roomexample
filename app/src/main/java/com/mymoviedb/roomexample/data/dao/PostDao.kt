package com.mymoviedb.roomexample.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mymoviedb.roomexample.model.Post
import io.reactivex.Completable

@Dao
interface PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: List<Post>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(post: Post): Long

    @Query("SELECT COUNT(*) FROM POSTS")
    suspend fun postCount(): Int
}