package com.mymoviedb.roomexample.presenter

import android.util.Log
import com.mymoviedb.roomexample.data.AppDatabase
import com.mymoviedb.roomexample.data.dao.PostDao
import com.mymoviedb.roomexample.data.dao.TimelineDao
import com.mymoviedb.roomexample.util.InitData
import com.mymoviedb.roomexample.views.contracts.TimelineView
import kotlinx.coroutines.*

class TimelinePresenter(private val view: TimelineView, database: AppDatabase) {

    private val timelineDao: TimelineDao = database.getTimelineDao()
    private val postDao: PostDao = database.getPostDao()
    val timelineLiveData = timelineDao.getTimeline()

    private val parentJob = Job()
    private val scope = CoroutineScope(parentJob + Dispatchers.IO)

    fun insertPost() {
        scope.launch {
            postDao.insert(InitData.newPost)
            view.postAdded()
        }
    }

    fun onDestroy() {
        parentJob.cancel()
    }

}