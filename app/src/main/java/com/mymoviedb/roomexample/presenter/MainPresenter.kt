package com.mymoviedb.roomexample.presenter

import android.util.Log
import com.mymoviedb.roomexample.data.AppDatabase
import com.mymoviedb.roomexample.util.InitData
import com.mymoviedb.roomexample.views.contracts.MainView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class MainPresenter(private val view: MainView, private val database: AppDatabase) {

    private val parentJob = Job()
    private val scope = CoroutineScope(parentJob + Dispatchers.IO)
    private val compositeDisposable = CompositeDisposable()
    private val postDao = database.getPostDao()
    private val userDao = database.getUserDao()

    fun hasDataSaved() {
        scope.launch {
            hasPostSaved()
        }
    }

    private suspend fun hasPostSaved() {
        if (postDao.postCount() > 0) {
            view.showTimeline()
        } else {
            saveData()
        }
    }

    private fun saveData() {
        insertUsers()
    }

    private fun insertUsers() {
        compositeDisposable.add(userDao.insert(InitData.users)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { insertPosts() }
        )
    }

    private fun insertPosts() {
        compositeDisposable.add(
            postDao.insert(InitData.posts)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { view.showTimeline() }
        )
    }

    fun onDestroy() {
        parentJob.cancel()
        compositeDisposable.dispose()
    }
}