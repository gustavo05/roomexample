package com.mymoviedb.roomexample

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import com.mymoviedb.roomexample.data.AppDatabase
import com.mymoviedb.roomexample.data.dao.CommentDao
import com.mymoviedb.roomexample.data.dao.PostDao
import com.mymoviedb.roomexample.data.dao.UserDao
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config


@RunWith(RobolectricTestRunner::class)
@Config(sdk = [21], manifest = Config.NONE)
class CommentDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var userDao: UserDao
    private lateinit var commentDao: CommentDao
    private lateinit var postDao: PostDao

    @Before
    fun setup() {
        val database = Room.inMemoryDatabaseBuilder(
            RuntimeEnvironment.application,
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        userDao = database.getUserDao()
        commentDao = database.getCommentDao()
        postDao = database.getPostDao()
    }

    @Test
    fun `validate comments query`() {
        saveData()
        val list = commentDao.getCommentsByPost(4)
        Assert.assertEquals(6, list.size)
    }


    private fun saveData() {
        userDao.insert(TestUtil.getListUser()).subscribe()
        postDao.insert(TestUtil.getListPosts()).subscribe()
        commentDao.insert(TestUtil.getComents())
    }
}