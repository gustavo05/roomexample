package com.mymoviedb.roomexample

import com.mymoviedb.roomexample.model.Comment
import com.mymoviedb.roomexample.model.Post
import com.mymoviedb.roomexample.model.User

object TestUtil {
    fun generateUser(): User {
        return User(0, "Gustavo", "Quesada", "quesada.tavo@gmail.com")
    }

    fun getListUser(): List<User> {
        return listOf(
            User(0, "Gustavo", "Quesada", "quesada.tavo@gmail.com"),
            User(0, "Test", "Example", "test@gmail.com")
        )
    }

    fun getListPosts(): List<Post> {
        return listOf(
            Post(0, 1, "First Post by Gustavo", "", 10),
            Post(0, 1, "Second Post by Gustavo", "", 10),
            Post(0, 1, "Third Post by Gustavo", "", 10),
            Post(0, 1, "First Post by Test", "", 20),
            Post(0, 1, "Second Post by Test", "", 20),
            Post(0, 1, "Third Post by Test", "", 20)
        )
    }

    fun getComents(): List<Comment> {
        return listOf(
            Comment(0, 1, 1, "Gustavo Comment to First Post", 0),
            Comment(0, 2, 1, "Second Comment to First Post", 0),
            Comment(0, 1, 2, "Gustavo Comment to Second Post", 0),
            Comment(0, 2, 3, "Test Comment to Third Post", 0),
            Comment(0, 1, 4, "Gustavo Comment", 0),
            Comment(0, 2, 4, "Test Comment", 0),
            Comment(0, 1, 4, "Gustavo Comment", 0),
            Comment(0, 2, 4, "Test Comment", 0),
            Comment(0, 2, 4, "Test Comment", 0),
            Comment(0, 1, 4, "Gustavo Comment", 10)
        )
    }
}