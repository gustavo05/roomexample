package com.mymoviedb.roomexample

import android.util.Log
import androidx.annotation.Nullable
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.room.Room
import com.mymoviedb.roomexample.data.AppDatabase
import com.mymoviedb.roomexample.data.dao.CommentDao
import com.mymoviedb.roomexample.data.dao.PostDao
import com.mymoviedb.roomexample.data.dao.TimelineDao
import com.mymoviedb.roomexample.data.dao.UserDao
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


@RunWith(RobolectricTestRunner::class)
@Config(sdk = [21], manifest = Config.NONE)
class TimelineDaoTest {

    private lateinit var userDao: UserDao
    private lateinit var commentDao: CommentDao
    private lateinit var postDao: PostDao
    private lateinit var timelineDao: TimelineDao

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        val database = Room.inMemoryDatabaseBuilder(
            RuntimeEnvironment.application,
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        userDao = database.getUserDao()
        commentDao = database.getCommentDao()
        postDao = database.getPostDao()
        timelineDao = database.getTimelineDao()
    }

    @Test
    fun `validate timeline view`(){
        saveData()
        val timeline = getValue(timelineDao.getTimeline())
        Log.d(TimelineDao::class.java.simpleName, timeline.toString())
        Assert.assertEquals(TestUtil.getListPosts().size, timeline.size)
    }


    private fun saveData() {
        userDao.insert(TestUtil.getListUser()).subscribe()
        postDao.insert(TestUtil.getListPosts()).subscribe()
        commentDao.insert(TestUtil.getComents())
    }

    @Throws(InterruptedException::class)
    fun <T> getValue(liveData: LiveData<T>): T {
        val data = arrayOfNulls<Any>(1)
        val latch = CountDownLatch(1)
        val observer = object : Observer<T> {
            override fun onChanged(@Nullable o: T) {
                data[0] = o
                latch.countDown()
                liveData.removeObserver(this)
            }
        }
        liveData.observeForever(observer)
        latch.await(2, TimeUnit.SECONDS)

        return data[0] as T
    }
}