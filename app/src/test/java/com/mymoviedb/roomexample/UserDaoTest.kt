package com.mymoviedb.roomexample

import androidx.room.Room
import com.mymoviedb.roomexample.data.AppDatabase
import com.mymoviedb.roomexample.data.dao.UserDao
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [21], manifest = Config.NONE)
class UserDaoTest {

    private lateinit var userDao: UserDao

    @Before
    fun setup() {
        val database = Room.inMemoryDatabaseBuilder(
            RuntimeEnvironment.application,
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        userDao = database.getUserDao()
    }

    @Test
    fun `insert user test`() {
        val newId = userDao.insert(TestUtil.generateUser())
        Assert.assertEquals(newId, 1)
    }

    @Test
    fun `validate on conflict strategy`() {
        userDao.insert(TestUtil.generateUser())
        userDao.insert(TestUtil.generateUser())
        Assert.assertEquals(1, userDao.getUsers().size)
    }

    @Test
    fun `update user test`() {
        val user = TestUtil.generateUser()
        val id = userDao.insert(user)
        user.id = id
        user.email = "test@gmail.com"
        userDao.update(user)
        val newUser = userDao.getUsers()[0]
        Assert.assertEquals( "test@gmail.com", newUser.email)
    }

    @Test
    fun `delete user test`() {
        val user = TestUtil.generateUser()
        user.id = userDao.insert(user)
        Assert.assertEquals( 1,userDao.getUsers().size)
        userDao.delete(user = user)
        Assert.assertEquals( 0,userDao.getUsers().size)
    }

}