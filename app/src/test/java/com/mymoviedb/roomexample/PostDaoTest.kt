package com.mymoviedb.roomexample

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import com.mymoviedb.roomexample.data.AppDatabase
import com.mymoviedb.roomexample.data.dao.PostDao
import com.mymoviedb.roomexample.data.dao.UserDao
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [21], manifest = Config.NONE)
class PostDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var userDao: UserDao
    private lateinit var postDao: PostDao

    @Before
    fun setup() {
        val database = Room.inMemoryDatabaseBuilder(
            RuntimeEnvironment.application,
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        userDao = database.getUserDao()
        postDao = database.getPostDao()
    }

    @Test
    fun `validate posts count query`() = runBlocking {
        saveData()
        val count = postDao.postCount()
        Assert.assertEquals(6, count)
    }


    private fun saveData() {
        userDao.insert(TestUtil.getListUser()).subscribe()
        postDao.insert(TestUtil.getListPosts()).subscribe()
    }
}